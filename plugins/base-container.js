import Vue from 'vue'
import BaseContainer from '@/components/base-container.vue'

Vue.component('base-container', BaseContainer)
