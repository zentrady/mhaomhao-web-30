FROM node:8.12.0-alpine

WORKDIR /usr/app

COPY package.json ./
COPY package-lock.json ./
RUN npm install

COPY . .
RUN npm run build
ENV HOST 0.0.0.0
EXPOSE 3000
CMD npm start
